cheatsheet do
  title 'My PHP Snippets'               # Will be displayed by Dash in the docset list
  docset_file_name 'my_php_snippets'    # Used for the filename of the docset
  keyword 'My PHP Snippets'             # Used as the initial search keyword (listed in Preferences > Docsets)
  # resources 'resources_dir'  # An optional resources folder which can contain images or anything else

  introduction '個人的なPHPメモ'  # Optional, can contain Markdown or HTML

  # A cheat sheet must consist of categories
  category do
    id 'よく使う関数'  # Must be unique and is used as title of the category

    entry do
      command 'dirname()'         # Optional
#       command 'CMD+SHIFT+N'   # Multiple commands are supported
      name '親ディレクトリのパスを返す'    # A short name, can contain Markdown or HTML
      notes <<-'END'
      #### 実行中のファイルディレクトリを取得
      ```php?start_inline=1
      $current_dir = dirname(__FILE__);
      ```
      END
    end
    entry do
      command 'CMD+W'
      name 'Close window'
    end
  end

  category do
    id 'よく使う構文'
    entry do
      name '呼び出し元のファイルパスを取得'
      notes <<-'END'
        ```php?start_inline=1
        //呼び出し元のファイル情報を取得
        $associative_array = debug_backtrace();
        //ファイル名を出力
        echo $associative_array[0]["file"];
        ```
        Or anything else **Markdown** or HTML.
      END
    end
  end

  notes 'Some notes at the end of the cheat sheet'
end
